const testimonials = [{
  "review": "This is a great place, the food is delicious and the atmosphere was really awesome.",
  "date": "2023-01-17"
},{
  "review": "You need to check the lemon dessert that's definitely the best dish on the menu.",
  "date": "2023-01-18"
},{
  "review": "The service was quick and so kind and the food was fresh, delicious and really fast.",
  "date": "2023-01-19"
},{
  "review": "To make a reservation online was really easy and in the restaurant all run perfectly.",
  "date": "2023-01-20"
},{
  "review": "I'm a frequent client and all the dishes are amazing, the atmosphere is really comfortable and the service always is really patient",
  "date": "2023-01-21"
}]

export default testimonials
