const dishes = [{
  name: 'baked-chicken-with-spinach',
  title: 'Chicken with spinach',
  price: 8.57,
  description: 'Baked chicken with spinach and tomatos well cooked',
  enableToOrder: false,
  picture: {
    name: 'food/baked chicken with spinach.jpeg',
    alt: 'baked chicken with spinach'
  }
},{
  name: 'baked-pumpkin',
  title: 'Baked Pumpkin',
  price: 3.45,
  description: 'A vegetarian dish to enjoy the flavor of an excellent pumpkin',
  enableToOrder: true,
  picture: {
    name: 'food/baked pumpkin.jpeg',
    alt: 'baked pumpkin'
  }
},{
  name: 'baked-salmon',
  title: 'Baked salmon',
  price: 9.45,
  description: 'Baked salmon with a lemon sauce with a perfect pinch of salt',
  enableToOrder: true,
  picture: {
    name: 'food/baked salmon.jpeg',
    alt: 'baked salmon'
  }
},{
  name: 'berry-cheesecake',
  title: 'Berry cheesecake',
  price: 4.75,
  description: 'A sweet cheesecake with berries',
  enableToOrder: false,
  picture: {
    name: 'food/berry cheesecake.jpeg',
    alt: 'berry cheesecake'
  }
},{
  name: 'chocolate-dessert',
  title: 'Chocolate dessert',
  price: 5.55,
  description: 'The exclusive dessert of lime chocolate',
  enableToOrder: true,
  picture: {
    name: 'food/chocolate dessert.jpeg',
    alt: 'chocolate dessert'
  }
},{
  name: 'deer-steak',
  title: 'Deer steak',
  price: 10.65,
  description: 'An excellent deersteak with potatoes',
  enableToOrder: true,
  picture: {
    name: 'food/deer steak.jpeg',
    alt: 'deer steak'
  }
},{
  name: 'dot-dessert',
  title: 'Dot dessert',
  price: 3.35,
  description: 'A vanilla dessert with dots of strawberries',
  enableToOrder: true,
  picture: {
    name: 'food/dot dessert.jpeg',
    alt: 'dot dessert'
  }
},{
  name: 'fried-beef',
  title: 'Fried beef',
  price: 5.75,
  description: 'a fired breef with a house´s selection spices',
  enableToOrder: false,
  picture: {
    name: 'food/fried beef.jpeg',
    alt: 'fried beef'
  }
},{
  name: 'greek-salad',
  title: 'Greek salad',
  price: 5.66,
  description: 'A greek salad with a lot of vegetables',
  enableToOrder: false,
  picture: {
    name: 'food/greek salad.jpg',
    alt: 'greek salad'
  }
},{
  name: 'ice-cream-with-brownies',
  title: 'Ice cream with brownies',
  price: 5.45,
  description: 'A vanilla ice cream scoop with brownies',
  enableToOrder: true,
  picture: {
    name: 'food/ice cream with  brownies.jpeg',
    alt: 'ice cream with  brownies'
  }
},{
  name: 'lemon-dessert',
  title: 'Lemon dessert',
  price: 6.15,
  description: 'The main desset of the house, "The Little Lemon Desset"',
  enableToOrder: true,
  picture: {
    name: 'food/lemon dessert.jpg',
    alt: 'lemon dessert'
  }
},{
  name: 'mushroom-cream',
  title: 'panna cotta with berries',
  price: 6.45,
  description: 'Mushroom cream with a lot of vegetables and chicken',
  enableToOrder: true,
  picture: {
    name: 'food/mushroom cream.jpeg',
    alt: 'mushroom cream'
  }
},{
  name: 'panna-cotta-with-berries',
  title: 'Panna cotta with berries',
  price: 6.75,
  description: 'Panna cotta with berries',
  enableToOrder: false,
  picture: {
    name: 'food/panna cotta with berries.jpeg',
    alt: 'panna cotta with berries'
  }
},{
  name: 'potatoes-and-chicken',
  title: 'Potatoes and chicken',
  price: 6.35,
  description: 'Potatoes with chicken with tomato sauce',
  enableToOrder: true,
  picture: {
    name: 'food/potatoes and chicken plate.jpeg',
    alt: 'potatoes and chicken plate'
  }
},{
  name: 'pumpkin-tomato-mincemeat',
  title: 'Pumpkin tomato mincemeat',
  price: 7.85,
  description: 'Pumpkin mincemeat with tomato',
  enableToOrder: true,
  picture: {
    name: 'food/pumpkin tomato mincemeat.jpeg',
    alt: 'pumpkin tomato mincemeat'
  }
},{
  name: 'roasted-meat',
  title: 'Roasted meat',
  price: 9.95,
  description: 'Roasted meat with tomato sauce and chicken',
  enableToOrder: true,
  picture: {
    name: 'food/roasted meat.jpeg',
    alt: 'roasted meat'
  }
},{
  name: 'salmon-with-vegetables',
  title: 'Salmon with vegetables',
  price: 11.35,
  description: 'Salmon with a lot vegetables',
  enableToOrder: false,
  picture: {
    name: 'food/salmon with vegetables.jpeg',
    alt: 'salmon with vegetables'
  }
},{
  name: 'traditional-fried-potatoes',
  title: 'Traditional fried potatoes',
  price: 6.49,
  description: 'Bowl of traditional fried potatoes with meat',
  enableToOrder: true,
  picture: {
    name: 'food/traditional fried potatoes.jpeg',
    alt: 'traditional fried potatoes'
  }
},{
  name: 'venison-steak',
  title: 'Venison steak',
  price: 12.15,
  description: 'An excellent steak with mashed potatoes',
  enableToOrder: true,
  picture: {
    name: 'food/venison steak.jpeg',
    alt: 'venison steak'
  }
},]

export default dishes
