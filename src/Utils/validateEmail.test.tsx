import { validateEmail } from "./validateemail";

test('expect email is right', () => {
  const result = validateEmail('test@gmail.com')
  expect(!!result).toBe(true)
});

test('expect email is wrong', () => {
  const result = validateEmail('green cats')
  expect(!!result).toBe(false)
});
