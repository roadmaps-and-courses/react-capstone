/* eslint-disable jsx-a11y/anchor-is-valid */
import PropTypes, { InferProps } from 'prop-types';
import './Card.scss'

function Card({picture, title, price, description, enableToOrder, onClick}: InferProps<typeof Card.propTypes>) {
  return (
    <div className="card menu-card">
      <div className="card-image">
        <figure className="image is-4by3">
          <img src={`${process.env.PUBLIC_URL}/stock/${picture.name}`} alt={picture.alt}/>
        </figure>
      </div>
      <div className="card-content">
        <div className="media">
          <div className="media-content">
            <p className="title is-4">{title}</p>
            <p className={` subtitle is-5 has-text-${enableToOrder || onClick ? 'primary' : 'danger'}`}>
              {enableToOrder || onClick ? `$${price}` : `out of stock`}
            </p>
          </div>
        </div>

        <div className="content">
          {description}
        </div>
      </div>
      { (enableToOrder && onClick ) && <footer className="card-footer">
        <a onClick={() => onClick({price, title})} className="card-footer-item">
          Add
        </a>
      </footer> }
    </div>
  )
}

Card.propTypes = {
  title: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  description: PropTypes.string.isRequired,
  enableToOrder: PropTypes.bool,
  onClick: PropTypes.func,
  picture: PropTypes.shape({
    name: PropTypes.string.isRequired,
    alt: PropTypes.string.isRequired,
  }).isRequired
}

export default Card;
