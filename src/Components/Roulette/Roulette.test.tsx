import { render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import testimonials from '../../Data/testimonials';
import Roulette from './Roulette';

test('renders the roulette', () => {
  const {container} = render(
    <MemoryRouter>
      <Roulette  content={testimonials} title="Reviews" hasClass="is-primary" />
    </MemoryRouter>
  )
  const linkElement = screen.getByText(/Reviews/i);
  expect(linkElement).toBeInTheDocument();
  const hero = container.getElementsByClassName('hero');
  expect(hero[0]).toHaveClass('is-primary')
});
