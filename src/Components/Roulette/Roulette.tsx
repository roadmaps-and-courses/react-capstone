import PropTypes, { InferProps } from 'prop-types';
import './Roulette.scss';

function Roulette({content, hasClass, title}: InferProps<typeof Roulette.propTypes>) {
  return (
    <div className={`hero ${hasClass != null ? hasClass : '' }`}>
      <div className="hero-body spaced">
        <div className='card-title-section'>
          <h1 className='title  is-1'>{title}</h1>
        </div>
        <div className="roulette-container">
          {
            content.map((element: any) => (
              <article className="message is-primary for-roulette" key={element.date}>
                <div className="message-body testimonial-content">
                  {element.review}
                </div>
              </article>
            ))
          }
        </div>
      </div>
    </div>
  )
}

Roulette.propTypes = {
  hasClass: PropTypes.string,
  title: PropTypes.string.isRequired,
  content: PropTypes.arrayOf(PropTypes.shape({
    review: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
  })).isRequired,
}

export default Roulette;
