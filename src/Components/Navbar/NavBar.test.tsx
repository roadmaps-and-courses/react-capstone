import { fireEvent, render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import Navbar from './Navbar';

test('renders the navbar', () => {
  const {container} = render(
    <MemoryRouter>
      <Navbar />
    </MemoryRouter>
  )
  const linkElement = screen.getByText(/More/i);
  expect(linkElement).toBeInTheDocument();
  const sliderElement = screen.getByRole(/button/i)
  fireEvent.click(sliderElement)
  const slider = container.getElementsByClassName('navbar-menu')
  expect(slider[0]).toHaveClass('is-active')
});
