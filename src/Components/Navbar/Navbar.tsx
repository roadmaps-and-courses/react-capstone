import { Link } from "react-router-dom";
import { useState } from "react";

import './Navbar.scss'

export default function Navbar() {
  const [showSlide, setShowSlide] = useState(false);

  const switchSlide = () => {
    setShowSlide(value => !value)
  }
  return (
    <nav className="navbar is-fixed-top" role="navigation" aria-label="main navigation">
      <div className={`nav-space spaced ${showSlide ? 'is-opened' : '' }`} >
        <div className="navbar-brand">
          <Link className="navbar-item" to="/">
            <img className="brand-logo" src={`${process.env.PUBLIC_URL}/logo.png`} alt="logo"/>
          </Link>
          <a role="button" onClick={switchSlide}
            className={`navbar-burger ${showSlide ? 'is-active' : '' }`} aria-label="menu"
            aria-expanded="false" data-target="navbarBasicExample">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </a>
        </div>
        <div id="navbarBasicExample"  className={`navbar-menu ${showSlide ? 'is-active' : '' }`}>
          <div className="navbar-start">
            <Link className="navbar-item" to="/">
              Home
            </Link>

            <Link className="navbar-item" to="/about">
              About
            </Link>

            <Link className="navbar-item" to="/menu">
              Menu
            </Link>

            <div className="navbar-item has-dropdown is-hoverable">
              <a className="navbar-link">
                More
              </a>
              <div className="navbar-dropdown">
                <Link className="navbar-item" to="/reservation">
                  Reservations
                </Link>
                <Link className="navbar-item" to="/order">
                  Order Online
                </Link>
                <hr className="navbar-divider"/>
                <Link className="navbar-item" to="/issue">
                  Report an issue
                </Link>
              </div>
            </div>
          </div>

          <div className="navbar-end">
            <div className="navbar-item">
              <div className="buttons">
              <a className="button is-primary" href="https://lemoncollections.com/collections/little-lemon" target="_blank">
                <strong>Sign up</strong>
              </a>
              <a className="button is-light" href="https://lemoncollections.com/collections/little-lemon" target="_blank">
                Log in
              </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>
  )
}
