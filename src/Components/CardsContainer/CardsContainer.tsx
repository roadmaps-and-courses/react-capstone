import PropTypes, { InferProps } from 'prop-types';
import Card from "../Card/Card";
import './CardContainer.scss';

function CardsContainer({content, hasClass, cardClick, title, button}: InferProps<typeof CardsContainer.propTypes>) {

  return (
    <div className={`hero ${hasClass != null ? hasClass : '' }`}>
      <div className="hero-body spaced">
        <div className='card-title-section'>
          <h1 className='title  is-1'>{title}</h1>
          { button && <button className={`button is-medium ${button?.hasClass != null ? button?.hasClass : '' }`} onClick={button?.onClick}>
            {button?.title}
          </button>}
        </div>
        <div className='card-container'>
          {
            content.map(({picture, name, title, price, description, enableToOrder} : any) => (
              <Card picture={picture} key={name} title={title}
                    price={price} description={description}
                    enableToOrder={enableToOrder} onClick={cardClick}/>
            ))
          }
        </div>
      </div>
    </div>
  )
}

CardsContainer.propTypes = {
  hasClass: PropTypes.string,
  cardClick: PropTypes.func,
  title: PropTypes.string.isRequired,
  button: PropTypes.shape({
    title: PropTypes.string.isRequired,
    hasClass:  PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
  }),
  content: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    description: PropTypes.string.isRequired,
    enableToOrder: PropTypes.bool,
    picture: PropTypes.shape({
      name: PropTypes.string.isRequired,
      alt: PropTypes.string.isRequired,
    }).isRequired,
  })).isRequired,
}

export default CardsContainer;
