import { render } from '@testing-library/react';
import Portainer from './Portainer';


test('renders the portainer', () => {
  const {container} = render(

      <Portainer hasClass="is-primary" pictures={[]}>
        <p>this is a child</p>
      </Portainer>

  )
  const hero = container.getElementsByClassName('hero');
  expect(hero[0]).toHaveClass('is-primary')
});
