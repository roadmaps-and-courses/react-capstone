import PropTypes, { InferProps } from 'prop-types';
import "./Portainer.scss";

function Portainer({hasClass, children, pictures}: InferProps<typeof Portainer.propTypes>) {
  return (
    <div className={`hero ${hasClass != null ? hasClass : '' }`}>
      <div className="hero-body columns has-text-centered spaced">
        <div className="column">
          {children}
        </div>
        <div className="column">
          <div className="image-group">{
            pictures.map(image => (
              <img src={`${process.env.PUBLIC_URL}/stock/${image.name}`} alt={image.alt} key={`key_${image.alt}`}/>
            ))
          }</div>
        </div>
      </div>
    </div>
  )
}

Portainer.propTypes = {
  hasClass: PropTypes.string,
  children: PropTypes.element.isRequired,
  pictures: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    alt: PropTypes.string.isRequired,
  }).isRequired).isRequired,
}

export default Portainer
