import PropTypes, { InferProps } from 'prop-types';
import Card from "../Card/Card";
import './CardsSelector.scss';

function CardsSelector({content, hasClass, cardClick, title, button}: InferProps<typeof CardsSelector.propTypes>) {

  return (
    <>
      <h1 className='title is-1'>{title}</h1>
      <h2 className='subtitle is-4'>Select your order from a collection of {content.length} dishes </h2>
      <div className='card-selector-container'>
        {
          content.map(({picture, name, title, price, description, enableToOrder} : any) => (
            <Card picture={picture} key={name} title={title}
                  price={price} description={description}
                  enableToOrder={enableToOrder} onClick={cardClick}/>
          ))
        }
      </div>
    </>
  )
}

CardsSelector.propTypes = {
  hasClass: PropTypes.string,
  cardClick: PropTypes.func,
  title: PropTypes.string.isRequired,
  button: PropTypes.shape({
    title: PropTypes.string.isRequired,
    hasClass:  PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
  }),
  content: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    description: PropTypes.string.isRequired,
    enableToOrder: PropTypes.bool,
    picture: PropTypes.shape({
      name: PropTypes.string.isRequired,
      alt: PropTypes.string.isRequired,
    }).isRequired,
  })).isRequired,
}

export default CardsSelector;
