import PropTypes, { InferProps } from 'prop-types';
import './GeneralSection.scss'

function GeneralSection({title, text, buttons}: InferProps<typeof GeneralSection.propTypes>) {
  return (
    <section className="section">
      <h1 className="title">{title}</h1>
      <h2 className="subtitle gs-content">
        {text}
      </h2>
      {
        buttons != null && <div className="centered-buttons">
          {
            buttons.map(button => (
              <button className={`button is-medium ${button?.hasClass != null ? button?.hasClass : '' }`} key={button?.title} onClick={button?.onClick}>
                {button?.title}
              </button>
            ))
          }
        </div>
      }
    </section>
  )
}

GeneralSection.propTypes = {
  title: PropTypes.string.isRequired,
  text:  PropTypes.string.isRequired,
  buttons: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string.isRequired,
    hasClass:  PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
  })),
}

export default GeneralSection;
