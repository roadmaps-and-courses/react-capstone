import { render, screen } from '@testing-library/react';
import GeneralSection from './GeneralSection';
import * as router from 'react-router';

const navigate = jest.fn()

beforeEach(() => {
  jest.spyOn(router, 'useNavigate').mockImplementation(() => navigate)
})


test('renders the portainer', () => {
  const {container} = render(

      <GeneralSection text="Little lemon opened on Thanksgiving Day 1994. Chef and Owner Carlos Torres began baking pies and selling them to restaurants and his neighbors out of a small kitchen at the corner of Hudson and North Moore St. in Tribeca.
      Today, NYC's beloved restaurant and pie shop celebrates 28 years of classic, made from scratch American cooking."
      title='Little Lemon Resturant'
      buttons={[{
        title: "Reserve a table",
        hasClass: "is-danger",
        onClick: () => navigate("/reserve")
      }, {
        title: "Order Online",
        hasClass: "is-link",
        onClick: () => navigate("/order")
      }]} />

  )
  const linkElement = screen.getByText(/Little Lemon Resturant/i);
  expect(linkElement).toBeInTheDocument();
});
