import { Link } from "react-router-dom";
import './Footer.scss'

export default function Footer() {
  return (
    <footer className="footer">
      <div className="content has-text-centered columns spaced">
        <div className="column">
          <img src={`${process.env.PUBLIC_URL}/footer.png`} alt="logo"/>
        </div>
        <div className="column footer-section">
          <h2 className="has-text-link">Navigation</h2>
          <ul className="has-text-white">
            <li><Link to="/">Home</Link></li>
            <li><Link to="/about">About</Link></li>
            <li><Link to="/menu">Menu</Link></li>
            <li><Link to="/reservation">Reservations</Link></li>
            <li><Link to="/order">Order online</Link></li>
            <li><Link to="/issue">Report issues</Link></li>
          </ul>
        </div>
        <div className="column footer-section">
          <h2 className="has-text-link">Contact us</h2>
          <ul className="has-text-white">
            <li>Home</li>
            <li>About</li>
            <li>Menu</li>
          </ul>
        </div>
        <div className="column footer-section">
          <h2 className="has-text-link">Follow us</h2>
          <ul className="has-text-white">
            <li><a href="https://twitter.com/LittlelemonsNFT" target="_blank">Twitter</a></li>
            <li><a href="https://www.instagram.com/littlelemon.mx" target="_blank">Instagram</a></li>
            <li><a href="https://www.facebook.com/Littlelemon.mx/" target="_blank">Facebook</a></li>
          </ul>
        </div>
      </div>
    </footer>
  )
}
