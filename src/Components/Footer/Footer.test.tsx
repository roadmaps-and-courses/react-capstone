import { render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import Footer from './Footer';

test('renders the footer', () => {
  render(
    <MemoryRouter>
      <Footer />
    </MemoryRouter>
  )
  const linkElement = screen.getByText(/Navigation/i);
  expect(linkElement).toBeInTheDocument();
});
