import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import { MemoryRouter } from 'react-router-dom';

test('renders the whole main page', () => {
  render(
    <MemoryRouter>
      <App />)
    </MemoryRouter>)
  const linkElement = screen.getByText(/Little Lemon Resturant/i);
  expect(linkElement).toBeInTheDocument();
});
