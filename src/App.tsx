import { Routes, Route } from "react-router-dom";

import ReservationPage from "./Screens/ReservationPage/ReservationPage";
import IssuesPage from "./Screens/IssuesPage/IssuesPage";
import OrderPage from "./Screens/OrderPage/OrderPage";
import AboutPage from "./Screens/AboutPage/AboutPage";
import ErrorPage from "./Screens/ErrorPage/ErrorPage";
import HomePage from "./Screens/HomePage/HomePage";
import MenuPage from "./Screens/MenuPage/MenuPage";
import Layout from "./Layout";
import './App.scss';

function App() {
  return (
    <Routes>
      <Route path="/" element={<Layout />}>
        <Route index element={<HomePage />} />
        <Route path="about" element={<AboutPage />} />
        <Route path="issue" element={<IssuesPage />} />
        <Route path="menu" element={<MenuPage />} />
        <Route path="order" element={<OrderPage />} />
        <Route path="reservation" element={<ReservationPage />} />
        <Route path="*" element={<ErrorPage />} />
      </Route>
    </Routes>
  );
}

export default App;
