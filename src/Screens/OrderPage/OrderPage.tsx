import { useState } from 'react'
import Swal from 'sweetalert2';
import CardsSelector from '../../Components/CardsSelector/CardsSelector';
import dishes from '../../Data/dishes';
import { validateEmail } from '../../Utils/validateemail';

interface OrderItem {
  title: string,
  price: number,
  quantity: number
}

export default function OrderPage() {
  const availableMenu = dishes.filter(dish => dish.enableToOrder)
  const [touched, setTouched] = useState({
    name: false,
    email: false,
    phone: false,
    address: false,
  })
  const initialOrder: Array<OrderItem> = []
  const [fullName, setFullName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [address, setAddress] = useState("");
  const [details, setDetails] = useState("");
  const [order, setOrder] = useState(initialOrder);

  const getIsFormValid = () => {
    return (
      fullName && phone  && order && address && order.length > 0 &&
      validateEmail(email)
    );
  };

  const clearForm = () => {
    setFullName("");
    setEmail("");
    setPhone("");
    setDetails("");
    setAddress("");
    setOrder([]);
  };

  const handleSubmit = (event: any) => {
    event.preventDefault();
    Swal.fire({
      icon: 'success',
      title: 'Thanks, ' + fullName,
      text: `Remerber to have $${order.reduce(
        (acc, item) => acc + item.price*item.quantity,
        0
      ).toFixed(2)} in effective to pay the delivery to the address:\r\n\r\n${address}`,
    })
    clearForm()
  };

  const returnInputState = (touched: boolean, isValid: boolean) =>  {
    return (!touched ) ? 'is-info' : (isValid) ? 'is-success' : 'is-danger';
  }

  const addItemToOrder = ({title, price}: OrderItem) => {
    let index = order.map((e: any) => e.title).indexOf(title);
    let temporalOrder = [...order];
    if (index === -1) {
      temporalOrder.push({title, price, quantity: 1})
    } else {
      temporalOrder[index].quantity += 1;
    }
    setOrder(temporalOrder)
  }

  const removeItemFromOrder = (title: string) => {
    setOrder(order => order.filter(or => (or.title !== title)))
  }

  return (
    <div className="hero">
      <div className="hero-body spaced has-text-centered">
        <div className="tile is-ancestor">
          <div className="tile is-vertical">
            <div className="tile">
              <div className="tile is-parent is-vertical is-4">
                <article className="tile is-child notification is-white">
                  <p className="title">Order here</p>
                  <form onSubmit={handleSubmit}>
                    <div className="field">
                      <label className="label">Name</label>
                      <div className="control">
                        <input className={`input ${returnInputState(touched.name, fullName.length > 0)}`} value={fullName}
                          onChange={(e) => setFullName(e.target.value)}
                          onFocus={(e) =>  setTouched(touched => ({...touched, name: true}))}
                          type="text" placeholder="Name" />
                      </div>
                    </div>

                    <div className="field">
                      <label className="label">Email</label>
                      <div className="control">
                        <input className={`input ${returnInputState(touched.email, !!validateEmail(email) )}`} type="email" value={email}
                          onChange={(e) => setEmail(e.target.value)}
                          onFocus={(e) => setTouched(touched => ({...touched, email: true}))}
                          placeholder="Email"/>
                      </div>
                    </div>

                    <div className="field">
                      <label className="label">Phone</label>
                      <div className="control">
                        <input className={`input ${returnInputState(touched.phone, phone.length > 0)}`} type="phone" value={phone}
                          onChange={(e) => setPhone(e.target.value)}
                          onFocus={(e) => setTouched(touched => ({...touched, phone: true}))}
                          placeholder="Phone"/>
                      </div>
                    </div>

                    <div className="field">
                      <label className="label">Address</label>
                      <div className="control">
                        <input className={`input ${returnInputState(touched.address, address.length > 0)}`} type="text" value={address}
                          onChange={(e) => setAddress(e.target.value)}
                          onFocus={(e) => setTouched(touched => ({...touched, address: true}))}
                          placeholder="Address"/>
                      </div>
                    </div>

                    <div className="field">
                      <label className="label">Details of the Order</label>
                      <div className="control">
                        <textarea value={details} onChange={(e) => { setDetails(e.target.value);}} className="textarea is-success" placeholder="Details of the Order"></textarea>
                      </div>
                    </div>
                    { order.length > 0 ? <table className="table">
                      <thead>
                        <tr>
                          <th>Dish</th>
                          <th>Price</th>
                          <th>Quantity</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th>Total</th>
                          <th>${order.reduce(
                            (acc, item) => acc + item.price*item.quantity,
                            0
                          ).toFixed(2)}</th>
                          <th>{order.reduce(
                            (acc, item) => acc + item.quantity,
                            0
                          )}</th>
                          <th>
                            <button type="button" className="button is-warning is-light" onClick={() => setOrder([])}>Remove All</button>
                          </th>
                        </tr>
                      </tfoot>
                      <tbody>
                        {order.map(orderItem => <tr key={orderItem.title}>
                          <th>{orderItem.title}</th>
                          <td>${orderItem.price}</td>
                          <td>{orderItem.quantity}</td>
                          <td>
                            <button type="button" className="button is-danger is-light" onClick={() => removeItemFromOrder(orderItem.title)}>
                              Remove
                            </button>
                          </td>
                        </tr>)}
                      </tbody>
                    </table> :
                    <h3 className='subtitle is-6 has-text-danger'>Please add at least one dish</h3>
                    }

                    <div className="field is-grouped is-grouped-centered">
                      <div className="control">
                        <button type="submit" disabled={!getIsFormValid()} className="button is-link">Submit</button>
                      </div>
                      <div className="control">
                        <button className="button is-link is-light"  type="button" onClick={clearForm}>Cancel</button>
                      </div>
                    </div>
                  </form>
                </article>
              </div>
              <div className="tile is-parent">
                <article className="tile is-child notification is-white">
                  <CardsSelector content={availableMenu} title="Available menu" cardClick={addItemToOrder} />
                </article>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
