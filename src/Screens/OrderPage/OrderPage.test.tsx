import { fireEvent, render, screen } from '@testing-library/react';
import OrderPage from './OrderPage';

test('renders the Order page', () => {
  render(
    <OrderPage />
  )
  const linkElement = screen.getByText(/Available menu/i);
  expect(linkElement).toBeInTheDocument();
});

test('the cancel button cleans the form', () => {
  render(
    <OrderPage />
  )
  const cancelButton = screen.getByText(/Cancel/i)
  expect(cancelButton).toHaveClass('is-light')
  fireEvent.click(cancelButton)
  const linkElement = screen.getByText(/Please add at least one dish/i);
  expect(linkElement).toBeInTheDocument();
});
