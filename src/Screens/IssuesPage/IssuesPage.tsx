export default function IssuesPage() {
  return (
    <div className="hero">
      <div className="hero-body spaced has-text-centered">
        <div className="tile is-ancestor">
          <div className="tile is-vertical">
            <div className="tile">
              <div className="tile is-parent is-vertical">
                <article className="tile is-child notification is-primary">
                  <p className="title">Do you have a problem with your order or reservation?</p>
                  <p className="subtitle">Please contact us through: <br/><br/><br/>
                    <strong className="has-text-white">customerService@littlelemon.com</strong><br/>
                    <strong className="has-text-white">1(646)5553890</strong>
                  </p>
                </article>
                <article className="tile is-child notification is-warning">
                  <p className="title">Do you have a problems with our web site?</p>
                  <p className="subtitle">please write an email to:<br/><br/><br/><strong>support@littlelemon.com</strong></p>

                </article>
              </div>
              <div className="tile is-parent">
                <article className="tile is-child notification is-info">
                  <p className="title">To Little Lemon</p>
                  <p className="subtitle">Provide the best experience is the first</p>
                  <figure className="image is-4by3">
                    <img src={`${process.env.PUBLIC_URL}/stock/restaurant chef B.jpg`} alt="chef B"/>
                  </figure>
                </article>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
