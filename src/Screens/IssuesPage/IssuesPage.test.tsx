import { render, screen } from '@testing-library/react';
import IssuesPage from './IssuesPage';


test('renders the Issues page', () => {
  render(
    <IssuesPage />
  )
  const linkElement = screen.getByText(/Do you have a problem with your order or reservation?/i);
  expect(linkElement).toBeInTheDocument();
});
