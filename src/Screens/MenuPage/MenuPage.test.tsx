import { fireEvent, render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import * as router from 'react-router'
import MenuPage from './MenuPage';

const navigate = jest.fn()

beforeEach(() => {
  jest.spyOn(router, 'useNavigate').mockImplementation(() => navigate)
})


test('renders the menu page', () => {
  render(
    <MemoryRouter>
      <MenuPage/>
    </MemoryRouter>
  )
  const linkElement = screen.getByText(/Menu/i);
  expect(linkElement).toBeInTheDocument();
});


test('the home button redirects to home page', () => {
  render(
    <MemoryRouter>
      <MenuPage/>
    </MemoryRouter>
  )
  const orderButton = screen.getByText(/Order Online/i)
  expect(orderButton).toHaveClass('is-link')
  fireEvent.click(orderButton)
  expect(navigate).toHaveBeenCalledWith('/order')
});
