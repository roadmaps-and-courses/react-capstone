import { useNavigate } from 'react-router-dom';

import CardsContainer from '../../Components/CardsContainer/CardsContainer'
import dishes from '../../Data/dishes';

export default function MenuPage() {
  const navigate = useNavigate();
  return (
    <CardsContainer content={dishes} title="Menu" button={{
      title: "Order Online",
      hasClass: "is-link",
      onClick: () => navigate("/order")
    }} />
  )
}
