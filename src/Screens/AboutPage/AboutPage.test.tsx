import { render, screen } from '@testing-library/react';
import AboutPage from "./AboutPage";

test('renders the about page', () => {
  render(
    <AboutPage />
  )
  const linkElement = screen.getByText(/Little Lemon Resturant/i);
  expect(linkElement).toBeInTheDocument();
});
