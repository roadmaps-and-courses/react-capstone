import React from 'react'
import GeneralSection from '../../Components/GeneralSection/GeneralSection'
import Portainer from '../../Components/Portainer/Portainer'
import mainData from '../../Data/mainData'

export default function AboutPage() {
  return (
    <Portainer pictures={mainData.pictures}>
      <GeneralSection
        text="Little lemon opened on Thanksqiving Day 1994. Chef and Owner Carlos Torres began baking pies and selling them to restaurants and his neighbors out of a small kitchen at the corner of Hudson and North Moore St. in Tribeca.
        Today, NYC's beloved restaurant and pie shop celebrates 28 years of classic, made from scratch American cooking."
        title='Little Lemon Resturant'
      />
    </Portainer>
  )
}
