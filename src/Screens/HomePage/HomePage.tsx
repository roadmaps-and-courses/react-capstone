import { useNavigate } from "react-router-dom";

import CardsContainer from '../../Components/CardsContainer/CardsContainer';
import GeneralSection from '../../Components/GeneralSection/GeneralSection';
import Portainer from '../../Components/Portainer/Portainer';
import Roulette from '../../Components/Roulette/Roulette';
import testimonials from "../../Data/testimonials";
import mainData from "../../Data/mainData"
import dishes from "../../Data/dishes";

export default function HomePage() {
  const navigate = useNavigate();
  const shuffled = dishes.sort(() => 0.5 - Math.random());
  return (
    <>
      <Portainer hasClass="is-primary" pictures={mainData.pictures}>
        <GeneralSection
          text="Little lemon opened on Thanksgiving Day 1994. Chef and Owner Carlos Torres began baking pies and selling them to restaurants and his neighbors out of a small kitchen at the corner of Hudson and North Moore St. in Tribeca.
          Today, NYC's beloved restaurant and pie shop celebrates 28 years of classic, made from scratch American cooking."
          title='Little Lemon Resturant'
          buttons={[{
            title: "Reserve a table",
            hasClass: "is-danger",
            onClick: () => navigate("/reserve")
          }, {
            title: "Order Online",
            hasClass: "is-link",
            onClick: () => navigate("/order")
          }]}
        />
      </Portainer>
      <CardsContainer content={shuffled.slice(0,4)} hasClass="is-info" title="Specials" button={{
          title: "See more",
          hasClass: "is-link",
          onClick: () => navigate("/menu")
        }} />
      <Roulette content={testimonials} title="Reviews"/>
    </>
  )
}
