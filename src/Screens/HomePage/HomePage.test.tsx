import { fireEvent, render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import * as router from 'react-router'
import HomePage from './HomePage';

const navigate = jest.fn()

beforeEach(() => {
  jest.spyOn(router, 'useNavigate').mockImplementation(() => navigate)
})


test('renders the home page', () => {
  render(
    <MemoryRouter>
      <HomePage />
    </MemoryRouter>
  )
  const linkElement = screen.getByText(/Reviews/i);
  expect(linkElement).toBeInTheDocument();
});



test('the buttons redirect correctly', () => {
  render(
    <MemoryRouter>
      <HomePage />
    </MemoryRouter>
  )
  //info button
  const reserveButton = screen.getByText(/Reserve a Table/i)
  fireEvent.click(reserveButton)
  expect(reserveButton).toHaveClass('is-danger')
  expect(navigate).toHaveBeenCalledWith('/reserve')
  //order button
  const orderButton = screen.getByText(/Order Online/i)
  fireEvent.click(orderButton)
  expect(orderButton).toHaveClass('is-link')
  expect(navigate).toHaveBeenCalledWith('/order')
  //menu button
  const menuButton = screen.getByText(/See more/i)
  fireEvent.click(menuButton)
  expect(menuButton).toHaveClass('is-link')
  expect(navigate).toHaveBeenCalledWith('/menu')
});
