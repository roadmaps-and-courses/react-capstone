import { fireEvent, render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import * as router from 'react-router'
import ErrorPage from './ErrorPage';

const navigate = jest.fn()

beforeEach(() => {
  jest.spyOn(router, 'useNavigate').mockImplementation(() => navigate)
})


test('renders the error page', () => {
  render(
    <MemoryRouter>
      <ErrorPage />
    </MemoryRouter>
  )
  const linkElement = screen.getByText(/Please check your request.../i);
  expect(linkElement).toBeInTheDocument();
});

test('the home button redirects to home page', () => {
  render(
    <MemoryRouter>
      <ErrorPage />
    </MemoryRouter>
  )
  const homeButton = screen.getByText(/Go Home/i)
  expect(homeButton).toHaveClass('is-warning')
  fireEvent.click(homeButton)
  expect(navigate).toHaveBeenCalledWith('/')
});
