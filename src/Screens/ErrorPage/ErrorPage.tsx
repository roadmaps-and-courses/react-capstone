import { useNavigate } from "react-router-dom";

export default function ErrorPage() {
  const navigate = useNavigate();
  return (
    <div className="hero">
      <div className="hero-body spaced has-text-centered">
        <h1 className='title is-1 has-text-primary'>Error 404</h1>
        <h2 className='subtitle is-2'>Please check your request...</h2>
        <button className="button is-medium is-warning" onClick={() => navigate("/")}>
          Go Home
        </button>
      </div>
    </div>
  )
}
