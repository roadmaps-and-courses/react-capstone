import { fireEvent, render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import ReservationPage from './ReservationPage';
import * as router from 'react-router'

const navigate = jest.fn()

beforeEach(() => {
  jest.spyOn(router, 'useNavigate').mockImplementation(() => navigate)
})


test('renders the Reservation page', () => {
  render(
    <MemoryRouter>
      <ReservationPage/>
    </MemoryRouter>
  )
  const linkElement = screen.getByText(/Make your reservation here/i);
  expect(linkElement).toBeInTheDocument();
});


test('the cancel button cleans the form', () => {
  render(
    <MemoryRouter>
      <ReservationPage/>
    </MemoryRouter>
  )
  const homeButton = screen.getByText(/Home/i)
  expect(homeButton).toHaveClass('is-light')
  fireEvent.click(homeButton)
  expect(navigate).toHaveBeenCalledWith('/')
});
