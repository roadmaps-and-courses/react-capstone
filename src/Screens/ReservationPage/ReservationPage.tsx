import { useNavigate } from 'react-router-dom';
import { useState } from 'react'
import Swal from 'sweetalert2';
import { validateEmail } from '../../Utils/validateemail';

export default function ReservationPage() {
  const navigate = useNavigate();
  const [touched, setTouched] = useState({
    name: false,
    email: false,
    phone: false,
    guests: false
  })

  const [fullName, setFullName] = useState("");
  const [guests, setGuests] = useState(0);
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [details, setDetails] = useState("");
  const [time, setTime] = useState("14:00");
  const [ocassion, setOcassion] = useState("Nothing special");
  const getIsFormValid = () => {
    return (
      fullName && phone  && guests > 0 &&
      validateEmail(email)
    );
  };

  const clearForm = () => {
    setFullName("");
    setEmail("");
    setPhone("");
    setDetails("");
    setTime("14:00");
    setGuests(0);
    setOcassion("Nothing special");
  };

  const handleSubmit = (event: any) => {
    event.preventDefault();
    Swal.fire({
      icon: 'success',
      title: 'Thanks, ' + fullName,
      text: `Your reservation to ${guests} persons at ${time} has been scheduled`,
    })
    clearForm()
  };

  const returnInputState = (touched: boolean, isValid: boolean) =>  {
    return (!touched ) ? 'is-info' : (isValid) ? 'is-success' : 'is-danger';
  }

  const handleTimeChange = (event: any) => {
    setTime(event.target.value);
  }

  const handleOcassionChange = (event: any) => {
    setOcassion(event.target.value);
  }

  return (
    <div className="hero">
      <div className="hero-body spaced has-text-centered">
        <div className="tile is-ancestor">
          <div className="tile is-vertical">
            <div className="tile">
              <div className="tile is-parent is-vertical">
                <article className="tile is-child notification is-white">
                  <p className="title">Make your reservation here</p>
                  <form onSubmit={handleSubmit}>
                    <div className="field">
                      <label className="label">Name</label>
                      <div className="control">
                        <input className={`input ${returnInputState(touched.name, fullName.length > 0)}`} value={fullName}
                          onChange={(e) => setFullName(e.target.value)}
                          onFocus={(e) =>  setTouched(touched => ({...touched, name: true}))}
                          type="text" placeholder="Name" />
                      </div>
                    </div>
                    <div className="field is-horizontal">
                      <div className="field-body">
                        <div className="field">
                          <div className="field">
                            <label className="label">Phone</label>
                            <div className="control">
                              <input className={`input ${returnInputState(touched.phone, phone.length > 0)}`} type="phone" value={phone}
                                onChange={(e) => setPhone(e.target.value)}
                                onFocus={(e) => setTouched(touched => ({...touched, phone: true}))}
                                placeholder="Phone"/>
                            </div>
                          </div>
                        </div>
                        <div className="field">
                          <div className="field">
                            <label className="label">Email</label>
                            <div className="control">
                              <input className={`input ${returnInputState(touched.email, !!validateEmail(email) )}`} type="email" value={email}
                                onChange={(e) => setEmail(e.target.value)}
                                onFocus={(e) => setTouched(touched => ({...touched, email: true}))}
                                placeholder="Email"/>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="field is-horizontal">
                      <div className="field-body">
                        <div className="field">
                          <div className="field">
                            <label className="label">Guests</label>
                            <div className="control">
                              <input className={`input ${returnInputState(touched.guests, guests > 0)}`} value={guests} min="1"
                                onChange={(e) => setGuests(+e.target.value)}
                                onFocus={(e) =>  setTouched(touched => ({...touched, guests: true}))}
                                type="number" placeholder="Guests" />
                            </div>
                          </div>
                        </div>
                        <div className="field">
                          <div className="field">
                            <label className="label">Time</label>
                            <div className="control">
                              <div className="select is-success">
                                <select value={time} onChange={handleTimeChange}>
                                  <option value="12:00">12:00</option>
                                  <option value="13:00">13:00</option>
                                  <option value="14:00">14:00</option>
                                  <option value="15:00">15:00</option>
                                  <option value="16:00">16:00</option>
                                  <option value="17:00">17:00</option>
                                  <option value="18:00">18:00</option>
                                  <option value="19:00">19:00</option>
                                  <option value="20:00">20:00</option>
                                  <option value="21:00">21:00</option>
                                  <option value="22:00">22:00</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="field">
                          <div className="field">
                            <label className="label">Ocassion</label>
                            <div className="control">
                              <div className="select is-success">
                                <select value={ocassion} onChange={handleOcassionChange}>
                                  <option value="Nothing special">Nothing special</option>
                                  <option value="Anniversary">Anniversary</option>
                                  <option value="Birthday">Birthday</option>
                                  <option value="Date">Date</option>
                                  <option value="Engagement">Engagement</option>
                                  <option value="Meeting">Meeting</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="field">
                      <label className="label">Details of the Reservation</label>
                      <div className="control">
                        <textarea value={details} onChange={(e) => { setDetails(e.target.value);}} className="textarea is-success" placeholder="Details of the reservation"></textarea>
                      </div>
                    </div>
                    <div className="field is-grouped is-grouped-centered">
                      <div className="control">
                        <button type="submit" disabled={!getIsFormValid()} className="button is-link">Submit</button>
                      </div>
                      <div className="control">
                        <button className="button is-link is-light"  type="button" onClick={() => navigate("/")}>Home</button>
                      </div>
                    </div>
                  </form>
                </article>
              </div>
              <div className="tile is-parent">
                <article className="tile is-child notification is-white">
                  <figure className="image is-4by3">
                    <img src={`${process.env.PUBLIC_URL}/stock/restaurant chef B.jpg`} alt="chef B"/>
                  </figure>
                </article>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
